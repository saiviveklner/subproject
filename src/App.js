import logo from "./logo.svg";
import "./App.css";
import InputFieldComponent from "./InputFieldComponent";
import "bootstrap/dist/css/bootstrap.min.css";
function App() {
  return (
    <div className="App">
      <InputFieldComponent />
    </div>
  );
}

export default App;
